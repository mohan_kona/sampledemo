import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'retrieve data from excel sheet'
def data = TestDataFactory.findTestData("Data Files/E2E flow kennismedewerker - 28-02-2018")
String URL = data.getValue(1, 1)
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'Enter valid credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'Send text to search bar'
CustomKeywords.'page_header_salesForce.headerUtilities.sendTextToSearchbar'("CS testuser1")
'click on button "Gebruikersgegevens"'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickGebruikersgegevens'()
'click on "Inloggen" button to ensure successfull login of the test user'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickInloggen'()
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Cases")
'click on first case'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'click on Feed tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_feed'))
'click on new task tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabNewTask/Icon_NewTask'))
'move to vervaldatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabNewTask/txtvervaldatum'))
'click on todays date'
String currentDate = CustomKeywords.'com.helper.CommonFunctions.functionLibrary.currentDate'(0)
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabNewTask/txtvervaldatum'), currentDate)
'click on save'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabNewTask/saveCase'))
'click on mainpage'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickOnHoofdpagina'()
'move to Tasks for today'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_Login_salesforce/page_hoofpagina_salesforce/ObjTodaysTask'))
'verify text in Tasks for today'
CustomKeywords.'com.helper.validation.validationNverification.verifyTextEqual'(findTestObject('OR_salesForce/page_Login_salesforce/page_hoofpagina_salesforce/clickTasksForToday'), "Vandaag")
'click anywhere in tasks for today'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_hoofpagina-salesForce/clickTasksForToday'))
'click on vervaldatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_Task_Details/clickOnVervaldatum'))
'change exisiting date'
String nextDate = CustomKeywords.'com.helper.CommonFunctions.functionLibrary.currentDate'(1)
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabNewTask/txtvervaldatum'), nextDate)
'click on Onderwerp field'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_Task_Details/txtOnderwerp'))
'select a value in Onderwerp dropdown'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_Task_Details/drpdwnOnderwerp'))
'click on save button'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_Task_Details/clickOpslaan'))
'close all tabs in header'
CustomKeywords.'page_header_salesForce.headerUtilities.closeAllTabs'()
'click on mainpage'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickOnHoofdpagina'()
'move to Tasks for today'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_Login_salesforce/page_hoofpagina_salesforce/ObjTodaysTask'))
'verify text in Tasks for today'
WebUI.verifyElementText(findTestObject('OR_salesForce/page_Login_salesforce/page_hoofpagina_salesforce/txtVerifyTask'), "Vandaag vervalt er niets. Wees erbij en kom spoedig terug.")
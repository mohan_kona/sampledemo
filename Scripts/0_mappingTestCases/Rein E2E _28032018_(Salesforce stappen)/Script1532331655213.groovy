import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable



'navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(AppURL)
'Enter valid credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(SalesForceSection)
'click on first element in case'
String caseNumber=CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'click on Intake in tab'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.clickOnCurrentStep'(currentStep)
'click on Huidige stap aanduiden als voltooid'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.markAsCurrentStep'()
'click on Informeren tab'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.clickOnCurrentStep'(nextCurrentStep)
'click on Huidige stap aanduiden als voltooid'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.markAsCurrentStep'()
'click on Feed tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_feed'))
'clicks on E-mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/clickEMail'))
'enter the recieptionist'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.sendReceptionist'(mailRecieptionist)
'enter TAB'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.performTab'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/recieptionistInput'))
'scroll to email image'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.emailBodyImage'()
'click on Knowledge bar'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.clickSearchBarKnowledge'()
'send text to knowledge bar'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.sendTextSearchBarKnowledge'(knowledgeValue)
'click on 2nd element "AH Bonus" in Knowledge DropDown'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.clickOnKnowledgeDrpDwn2'()
'click on subtab by name'
CustomKeywords.'page_header_salesForce.headerUtilities.clickSubTabByName'(caseNumber)
'click on Knowledge bar'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.clickSearchBarKnowledge'()
'perform enter on Knowledge bar'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.performEnter'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/KnowledgeTab/searchKnowledge'))
'click on dropdpwn present beside the 2nd article'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.btnKnowledgeArticleDrpDwn'()
'click  on Artikel_loskoppelen'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.clickOnArtikel_loskoppelen'()
'click on Informeren tab'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.clickOnCurrentStep'(finalStep)
'click on Huidige stap aanduiden als voltooid'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.markAsCurrentStep'()
 

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def data = TestDataFactory.findTestData("Data Files/E2E flow KS medewerker Mail - 22-02-2018(1)")
String URL = data.getValue(1, 1)
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on search bar present on header'
CustomKeywords.'page_header_salesForce.headerUtilities.clickOnSearchBar'()
'searching a particular testuser by sending testuser name to the search bar to search'
CustomKeywords.'page_header_salesForce.headerUtilities.searchDrpdwn'("CS Testuser26")
'click on button "Gebruikersgegevens"'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickGebruikersgegevens'()
'click on "Inloggen" button to ensure successfull login of the test user'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickInloggen'()
'click on "Klanten"'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Klanten")
'click on "Recent weergegeven" under Klanten'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.selectSubTypeInDropDown'("Recent weergegeven")
'click on 1st klant'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'click on case in the klant page'
String klantName=CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/CaseTab/clickOnFirstCase'))
'disable left case Details tab'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.comboBox'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/disableCSJ'),'Customer Service Journey')
'click on Banner on left case Details tab'
List<WebElement> leftTabEle = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/bannerInfoFields'))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSEXecutorElement'(leftTabEle.get(0))
'change the banner value'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'("AH BE")
'click on Customer service journey on left tab'
List<WebElement> leftTabEle1 = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/CSJinfoFields'))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSEXecutorElement'(leftTabEle1.get(0))
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'('Actie/thema AH BE')
'click save'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
'verify error txt'
WebUI.verifyEqual(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/invalidTxtBanner'), "De banner op de case komt niet overeen met de banner op de klant")
'change banner value again'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSEXecutorElement'(leftTabEle.get(0))
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'("AH NL")
'click save'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
'click on sub tab to verify the klant banner'
CustomKeywords.'page_header_salesForce.headerUtilities.clickSubTabByName'(klantName)
'move to banner header to verify'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_CustomerInfo_salesForce/CustomerInfo/getBannerValue'))
WebUI.verifyNotEqual(findTestObject('Object Repository/OR_salesForce/page_CustomerInfo_salesForce/CustomerInfo/getBannerValue'), "AH BE")
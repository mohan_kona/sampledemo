import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def data = TestDataFactory.findTestData("Data Files/E2E flow KS medewerker Mail - 22-02-2018(1)")
String URL = data.getValue(1, 1)
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on search bar present on header'
CustomKeywords.'page_header_salesForce.headerUtilities.clickOnSearchBar'()
'searching a particular testuser by sending testuser name to the search bar to search'
CustomKeywords.'page_header_salesForce.headerUtilities.searchDrpdwn'("CS Testuser14")
'click on button "Gebruikersgegevens"'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickGebruikersgegevens'()
'click on "Inloggen" button to ensure successfull login of the test user'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickInloggen'()
'click on reports'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Rapporten")
'click on "all reports"'
CustomKeywords.'page_rapporten_salesForce.reportsDetails.reportCategory'("Alle rapporten")


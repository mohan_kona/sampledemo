import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'retrieve data from excel sheet'
def data = TestDataFactory.findTestData("Data Files/E2E flow kennismedewerker - 28-02-2018")
String URL = data.getValue(1, 1)
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'Enter valid credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Klanten")
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'click on volgende button'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_CustomerInfo_salesForce/createKlanten/btnVolgende'))
'click on drpoDown KlantNaam'
List<WebElement> klantDetails = WebUiBuiltInKeywords.findWebElements(findTestObject('OR_salesForce/page_CustomerInfo_salesForce/createKlanten/klantDropdown'),10)
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(klantDetails.get(0))
'select value from dropdown klantnaam'
CustomKeywords.'page_CustomerInfo_salesForce.createKlanten.selectValueFromDropdown'("Dhr.")
'send text to Achternaam'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('OR_salesForce/page_CustomerInfo_salesForce/createKlanten/txtAchternaam'), "Gojira")
'move to E-mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_CustomerInfo_salesForce/createKlanten/txtEmail'))
'send text to email'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('OR_salesForce/page_CustomerInfo_salesForce/createKlanten/txtEmail'), "gojira@gmail.com")
'move to banner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_CustomerInfo_salesForce/createKlanten/drpdwnBanner'))
'click on drpoDown KlantNaam'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(klantDetails.get(3))
'select value from dropdown klantnaam'
CustomKeywords.'page_CustomerInfo_salesForce.createKlanten.selectValueFromDropdown'("NLD")
'click on Opslaan'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('OR_salesForce/page_CustomerInfo_salesForce/createKlanten/txtOpslaan'))
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Cases")
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'select record type'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.selectRecordType'()
'Click on "to next"'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.clickOnVolgende'()
'move to additional info tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/objWebgegevens'))
'change Customer Service Journey'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.selectValueFromDropdown'("Actie/thema AH NL")
'change case origin'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.selectValueFromDropdown'("Website")
'move to email tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/txtEmail'))
'send text to email tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/txtEmail'), "gojira@gmail.com")
'click on save button'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
'click on details tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/Icon_details'))
'move to caseNumber in cases tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/getCaseNumber'))
'get the case number in cases tab'
String caseNumberCase = WebUI.getText(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/getCaseNumber'))
'click on subtab named Gojira'
CustomKeywords.'page_header_salesForce.headerUtilities.clickTabByName'("Gojira")
'get the case number in persoonaccount tab'
String caseNumberPerson = WebUI.getText(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/CaseTab/clickOnFirstCase'))
'verify the case numbers are equal or not in case tab and personaccount tab'
CustomKeywords.'com.helper.validation.validationNverification.verifyObjectEqual'(caseNumberCase, caseNumberPerson)
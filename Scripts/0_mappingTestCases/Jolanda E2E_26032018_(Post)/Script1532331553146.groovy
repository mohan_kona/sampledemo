import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)

'Enter valid credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)

'click on klanten'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickOnKlanten'()

'click on search bar'
CustomKeywords.'page_header_salesForce.headerUtilities.clickOnSearchBar'()

'Send text to search bar'
CustomKeywords.'page_header_salesForce.headerUtilities.sendTextToSearchbar'(klantName)

'perform enter'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.performEnter'(findTestObject('Object Repository/OR_salesForce/page_header_salesForce/searchBar'))

'click on contact person'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_header_salesForce/contactPersonInSearch'))

'click on first case'
CustomKeywords.'page_contactPersons_salesForce.CaseTab.clickonFirstCase'()

'click on Feed tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_feed'))

'clicks on E-mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/clickEMail'))

'enter the recieptionist'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.sendReceptionist'(mailReceptionist)

'enter TAB'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.performTab'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/recieptionistInput'))

'scroll to email image'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.emailBodyImage'()

'click on search bar'
CustomKeywords.'page_header_salesForce.headerUtilities.clickOnSearchBar'()

'Send text to search bar'
CustomKeywords.'page_header_salesForce.headerUtilities.sendTextToSearchbar'(contactPerson)

'perform enter'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.performEnter'(findTestObject('Object Repository/OR_salesForce/page_header_salesForce/searchBar'))

'click on contact person'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_header_salesForce/contactPersonSashaBonk'))

'click on cases tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/CaseTab/clickOnCasesLink'))

'click on first case'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_Case/clickOnFirstCase'))

'click on Feed tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_feed'))

'move to all updates'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_allUpdates'))


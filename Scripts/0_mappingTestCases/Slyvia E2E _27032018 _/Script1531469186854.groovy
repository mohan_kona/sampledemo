import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)

'Enter valid credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)

'click on customer'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(KlantSalesForceSection)

'select the drop down to filter the case types'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnRecentActivtiesDropDown'()

'go to recent activities'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.selectSubTypeInDropDown'(subCategory_SFSection)

'click on the first element'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()

'click on new case'
CustomKeywords.'page_CustomerInfo_salesForce.headerDetails.clickOnNewCase'()

'click on case origin'
CustomKeywords.'page_CustomerInfo_salesForce.newCaseDialogBox.clickOnCaseherkomst'()

'select the case origin'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'('//div[@class=\'select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short visible positioned\']/div[@class=\'select-options\']/ul/li[3]')

'click on status'
CustomKeywords.'page_CustomerInfo_salesForce.newCaseDialogBox.clickOnStatus'()

'select the status'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'('//div[@class=\'select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short visible positioned\']/div[@class=\'select-options\']/ul/li[3]')

'click on save'
CustomKeywords.'page_CustomerInfo_salesForce.newCaseDialogBox.clickOnOpslaan'()

'click on the case assigned to customer'
CustomKeywords.'page_contactPersons_salesForce.CaseTab.clickonFirstCase'()

'Click on Feed tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_feed'))

'set CSJ'
WebDriver driver = DriverFactory.getWebDriver()

'disable left tab'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.comboBox'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/disableCSJ'), 
    'Customer Service Journey')

'click on Customer service journey on left tab'
List<WebElement> leftTabEle = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/CSJinfoFields'), 
    30)

CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(leftTabEle.get(0))

CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(CSJ)

'click Category'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(leftTabEle.get(1))

'change category'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(category)

'change Status'
List<WebElement> TabEle = driver.findElements(By.xpath('//div[@class="main-container slds-grid"]//div[@class="slds-form slds-is-editing"]/div/div[1]//a'))

CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(TabEle.get(1))

CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(status)

'click save'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()

'set the current step'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.clickOnCurrentStep'(currentStep)

'clicks on E-mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/clickEMail'))

'close the exsisting recieptionist'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/closeExsistingRecieptionist'))

'enter the recieptionist'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.sendReceptionist'(mailReceptionist)

'enter TAB'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.performTab'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/sendReceptionist'))

'scroll to send button'
WebUI.scrollToElement(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/btnToSend'), 
    10)

'clicks on send mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/btnToSend'))

'disable left tab'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.comboBox'('//div[@class="main-container slds-grid"]/div[1]/div[1]//div[@class="riseTransitionEnabled"]//div[@class="slds-form "]/div', 
    'Customer Service Journey')

'change Status'
List<WebElement> TabEle1 = driver.findElements(By.xpath('//div[@class="main-container slds-grid"]//div[@class="slds-form slds-is-editing"]/div/div[1]//a'))

CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(TabEle1.get(1))

CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(case1Status)

'click save'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()

'mark as current Step'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.markAsCurrentStep'()

'clicks on E-mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/clickEMail'))

'close the exsisting recieptionist'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/closeExsistingRecieptionist'))


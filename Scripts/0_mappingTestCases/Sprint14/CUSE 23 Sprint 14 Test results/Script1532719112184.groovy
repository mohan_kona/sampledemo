import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'retrieve data from excel sheet'
def data = TestDataFactory.findTestData("Data Files/E2E flow kennismedewerker - 28-02-2018")
String URL = data.getValue(1, 1)
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'Enter valid credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'Send text to search bar'
CustomKeywords.'page_header_salesForce.headerUtilities.sendTextToSearchbar'("CS testuser1")
'click on button "Gebruikersgegevens"'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickGebruikersgegevens'()
'click on "Inloggen" button to ensure successfull login of the test user'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickInloggen'()
'click on "klanten dropdown" and then click on "klanten" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Klanten")
'click on 1st contanct person in klanten'
String personName = CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'click on btnGeboortedatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnGeboortedatum'))
'send text to Geboortedatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/txtGeboortedatum'), "")
//Yesterday's date
'change the exixting to yesterday date'
String yesterdayDate = CustomKeywords.'com.helper.CommonFunctions.functionLibrary.currentDate'(-1)
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/txtGeboortedatum'), yesterdayDate)
'click on save'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnsave'))
'close the current tab'
CustomKeywords.'page_header_salesForce.headerUtilities.closeTabByName'(personName)
//change to todays date
'click on main tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickMainTab'()
'click on 1st contanct person in klanten'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'compare the texts in the alert box'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/txtVerifyBirthday'), "Hitler Adof is morgen/vandaag/gister jarig")
'click on btnGeboortedatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnGeboortedatum'))
'send text to Geboortedatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/txtGeboortedatum'), "")
'change the exixting to tomorrows date'
String todaysDate = CustomKeywords.'com.helper.CommonFunctions.functionLibrary.currentDate'(0)
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/txtGeboortedatum'), tomorrowsDate)
'click on save'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnsave'))
'close the current tab'
CustomKeywords.'page_header_salesForce.headerUtilities.closeTabByName'(personName)
'click on main tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickMainTab'()
'click on 1st contanct person in klanten'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'compare the texts in the alert box'
CustomKeywords.'com.helper.validation.validationNverification.verifyTextEqual'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/txtVerifyBirthday'), "Hitler Adof is morgen/vandaag/gister jarig")
//Change to tomorrows date
'click on main tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickMainTab'()
'click on 1st contanct person in klanten'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'compare the texts in the alert box'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/txtVerifyBirthday'), "Hitler Adof is morgen/vandaag/gister jarig")
'click on btnGeboortedatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnGeboortedatum'))
'send text to Geboortedatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/txtGeboortedatum'), "")
'change the exixting to tomorrows date'
String tomorrowsDate = CustomKeywords.'com.helper.CommonFunctions.functionLibrary.currentDate'(1)
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/txtGeboortedatum'), tomorrowsDate)
'click on save'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnsave'))
'close the current tab'
CustomKeywords.'page_header_salesForce.headerUtilities.closeTabByName'(personName)
'click on main tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickMainTab'()
'click on 1st contanct person in klanten'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'compare the texts in the alert box'
CustomKeywords.'com.helper.validation.validationNverification.verifyTextEqual'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/txtVerifyBirthday'), "Hitler Adof is morgen/vandaag/gister jarig")
//left blank
'click on main tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickMainTab'()
'click on 1st contanct person in klanten'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'compare the texts in the alert box'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/txtVerifyBirthday'), "Hitler Adof is morgen/vandaag/gister jarig")
'click on btnGeboortedatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnGeboortedatum'))
'send text to Geboortedatum'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/txtGeboortedatum'), "")
'click on save'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnsave'))
'close the current tab'
CustomKeywords.'page_header_salesForce.headerUtilities.closeTabByName'(personName)
'click on main tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickMainTab'()
'click on 1st contanct person in klanten'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'compare the texts in the alert box'
CustomKeywords.'com.helper.validation.validationNverification.verifyTextEqual'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/txtVerifyBirthday'), "Hitler Adof is morgen/vandaag/gister jarig")

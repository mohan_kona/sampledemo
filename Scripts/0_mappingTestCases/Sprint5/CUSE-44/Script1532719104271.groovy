import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'retrieve data from excel sheet'
def data = TestDataFactory.findTestData("Data Files/E2E flow kennismedewerker - 28-02-2018")
String URL = data.getValue(1, 1)
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'Enter valid credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickOnCases'()
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'Click on Volgende'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.clickOnVolgende'()
'mouse hover to casemanager tooltip'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.caseTooltipNavigation'("Casemanager")
'validation point for casemanager tooltip'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/caseManagerTooltip'), "Primair contact voor deze klant")
'mouse hover to banner tooltip'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.caseTooltipNavigation'("Banner")
'validation point for banner tooltip'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/bannerTooltip'), "Kies de banner waarmee de klant contact heeft gezocht")
'mouse hover to customer service journey tooltip'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.caseTooltipNavigation'("Customer Service Journey")
'validation point for customer service journey tooltip'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/customerServiceJourneyTooltip'), "Waarover neemt deze klant contact op?")
'click on "kennis dropdown" and then click on "kennis" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickOnKennis'()
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'mouse hover to Instructie tooltip'
CustomKeywords.'page_complaintCaseCollection_salesForce.KnowledgeMaking.kennisTooltipNavigation'("Instructie")
'validation point for Instructie tooltip'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/knowledgeMaking/instructieTooltip'), "Instructie voor afhandeling van deze case ")
'mouse hover to Externe kennis tooltip'
CustomKeywords.'page_complaintCaseCollection_salesForce.KnowledgeMaking.kennisTooltipNavigation'("Externe kennis")
'validation point for Externe kennis tooltip'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/knowledgeMaking/externeKennisTooltip'), "Kennis voor communicatie met individuele klant")
'mouse hover to Publieke kennis tooltip'
CustomKeywords.'page_complaintCaseCollection_salesForce.KnowledgeMaking.kennisTooltipNavigation'("Publieke kennis")
'validation point for Publieke kennis tooltip'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/knowledgeMaking/publiekeKennisTooltip'), "Kennis voor communicatie via portal/Internet")
'click on "klant dropdown" and then click on "klant" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickOnKlanten'()
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'Click on Volgende'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.clickOnVolgende'()
'mouse hover to Parley tooltip'
CustomKeywords.'page_complaintCaseCollection_salesForce.KnowledgeMaking.kennisTooltipNavigation'("Parley")
'validation point for Parley tooltip'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/klanten/parleyTooltip'), "Parley")
'mouse hover to Social Influencer Score tooltip'
CustomKeywords.'page_complaintCaseCollection_salesForce.KnowledgeMaking.kennisTooltipNavigation'("Social Influencer Score")
'validation point for Social Influencer Score tooltip'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/klanten/socialInfluenceScoreTooltip'), "Social Influencer Score")

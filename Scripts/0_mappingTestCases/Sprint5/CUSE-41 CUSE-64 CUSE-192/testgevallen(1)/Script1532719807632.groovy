import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'retrieve data from excel sheet'
def data = TestDataFactory.findTestData("Data Files/NewCaseCreation")
String URL = data.getValue(1, 1)
println URL
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
String custName = data.getValue(4, 1)
String contactPerson = data.getValue(5, 1)
'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Cases")
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'select record type'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.selectRecordType'()
'Click on "to next"'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.clickOnVolgende'()
'move to element'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/ObjAanvullendeGegevens'))
'provide case data'
List<WebElement> caseData = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/caseFieldInfoElements'),10)
println caseData.size()
'change klant(customer) name'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.sendCaseDetails'(caseData, custName)
'change contact person'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.sendCaseDetails'(caseData, contactPerson)
'move to additional info tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/objWebgegevens'))
'add additional info'
List<WebElement> additionalData = WebUiBuiltInKeywords.findWebElements(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/AdditionalCasefieldInfoElements'),10)
'change banner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(0))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('null'))
'change Customer Service Journey'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(1))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/changeCSJ'))
'change status'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(2))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/changeStatus'))
'change categorie'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(3))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('null'))
'change case origin'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(4))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('null'))
'click on save button'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
'verify klant SLA is present or not'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/mijlpalenTab/txtKlantSLA'), "Klant SLA")
'click on change owner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnChangeOwner'))
'click on Wachtrijen(Quees)'
CustomKeywords.'page_complaintCaseCollection_salesForce.ChangeOwner.clickOwnerWachtrijen'()
'send text to Wachtrijen search bar'
CustomKeywords.'page_complaintCaseCollection_salesForce.ChangeOwner.sendText'("AH T2 Cat-Bier")
'click on submit button'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnSubmit'))
'click on meer tonen in mijlpalen'
CustomKeywords.'page_caseInfo_salesForce.mijlpalen.clickOnMeerTonen'()
'verify owner SLA is present or not'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/mijlpalenTab/txtOwnerSLA'), "Owner SLA")
'click on change owner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnChangeOwner'))
'send text to mensen search bar'
CustomKeywords.'page_complaintCaseCollection_salesForce.ChangeOwner.sendText'("CS Testuser1")
'click on submit button'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnSubmit'))
'verify owner SLA text completed or not'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/mijlpalenTab/txtOwnerSLACompleted'), "Completed")

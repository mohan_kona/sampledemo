import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'retrieve data from excel sheet'
def data = TestDataFactory.findTestData("Data Files/NewCaseCreation")
String URL = data.getValue(1, 1)
println URL
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on search bar present on header'
CustomKeywords.'page_header_salesForce.headerUtilities.clickOnSearchBar'()
'Send text to search bar'
CustomKeywords.'page_header_salesForce.headerUtilities.searchDrpdwn'("Store manager Winkel")
'click on button "Gebruikersgegevens"'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickGebruikersgegevens'()
'click on "Inloggen" button to ensure successfull login of the test user'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickInloggen'()
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Cases")
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'select record type'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.selectRecordType'()
'Click on "to next"'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.clickOnVolgende'()
'send text to Onderwerp'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/txtOnderwerp'), "Store with store manager")
'move to element'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/ObjAanvullendeGegevens'))
'click on winkel'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/txtstoreWinkel'))
'send text to winkel'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.winkelSearchDropdown'("Adriaen van Ostadelaan-7777")
'move to additional info tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/objWebgegevens'))
'click on caseherkomst'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/txtCaseOrigin'))
'change value in caseherkomst'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.selectValueFromDropdown'("Winkel")
'click on save'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
'verify if knowledge tab is not present'
WebUI.verifyElementNotPresent(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/KnowledgeTab/searchKnowledge'), 5)
'logout as Store manager winkle'
CustomKeywords.'page_header_salesForce.headerUtilities.clickLogOut'()
'click on search bar present on header'
CustomKeywords.'page_header_salesForce.headerUtilities.clickOnSearchBar'()
'Send text to search bar'
CustomKeywords.'page_header_salesForce.headerUtilities.searchDrpdwn'("CS testuser1")
'click on button "Gebruikersgegevens"'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickGebruikersgegevens'()
//'click on "Inloggen" button to ensure successfull login of the test user'
//CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickInloggen'()
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Cases")
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'select record type'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.selectRecordType'()
'Click on "to next"'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.clickOnVolgende'()
'send text to Onderwerp'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/txtOnderwerp'), "Store with CS Testuser")
'move to additional info tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/objWebgegevens'))
'add additional info'
List<WebElement> additionalData = WebUiBuiltInKeywords.findWebElements(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/AdditionalCasefieldInfoElements'),10)
'change Customer Service Journey'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(1))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'("Bestelling ontvangen AH NL")
'change case origin'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(4))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'('Winkel')
'click on save'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
'verify knowledge is present '
WebUI.verifyElementPresent(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/KnowledgeTab/searchKnowledge'), 5)

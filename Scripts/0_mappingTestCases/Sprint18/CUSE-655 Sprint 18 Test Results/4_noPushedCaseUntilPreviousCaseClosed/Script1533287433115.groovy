import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

def data = TestDataFactory.findTestData("Data Files/E2E flow KS medewerker Mail - 22-02-2018(1)")
String URL = data.getValue(1, 1)
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on search bar present on header'
CustomKeywords.'page_header_salesForce.headerUtilities.clickOnSearchBar'()
'searching a particular testuser by sending testuser name to the search bar to search'
CustomKeywords.'page_header_salesForce.headerUtilities.searchDrpdwn'("CS Testuser14")
'click on button "Gebruikersgegevens"'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickGebruikersgegevens'()
'click on "Inloggen" button to ensure successfull login of the test user'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickInloggen'()
'count the number of tabs in open'
String initialTabCount = CustomKeywords.'com.helper.validation.validationNverification.countElements'('//*[@id="oneHeader"]//div[@class="tabBar slds-grid"]/ul[2]/li')
'click on omni-channel'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.clickOmniChannel'()
'click on drop down to change status'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.tiggerOmnichannelDrpdwn'()
'set Status to Online'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.setOmnichannelStatus'("Beschikbaar")
'check cases in mijn werk'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.clickPushedCaseIn'("Mijn werk")


'click on omni-channel'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.clickOmniChannel'()
'go to cases'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Cases")
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'select record type'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.selectRecordType'()
'Click on "to next"'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.clickOnVolgende'()
'move to element'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/ObjAanvullendeGegevens'))
'provide case data'
List<WebElement> caseData = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/caseFieldInfoElements'),10)
println caseData.size()
'change klant(customer) name'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.sendCaseDetails'(caseData, custName)
'change contact person'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.sendCaseDetails'(caseData, contactPerson)
'move to additional info tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/objWebgegevens'))
'add additional info'
List<WebElement> additionalData = WebUiBuiltInKeywords.findWebElements(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/AdditionalCasefieldInfoElements'),10)
'change banner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(0))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'("AH NL")
'change Customer Service Journey'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(1))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'("Actie/thema AH BE")
'change status'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(2))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'("In behandeling")
'change categorie'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(3))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'("Bonusfolder")
'change case origin'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(4))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'('Telefoon')
'click on save button'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
'go to details tab to fetch the current casenumber'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/Icon_details'))
'Fetching case number in the details page storing into a string'
WebUI.scrollToElement(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/getCaseNumber'), 10)
String actualCaseNumber = WebUI.getText(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/getCaseNumber'))
'close all the opened cases'
CustomKeywords.'page_header_salesForce.headerUtilities.closeAllTabs'()
//verify that new case created exists in omni channel


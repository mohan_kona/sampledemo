import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'retrieve data from excel sheet'
def data = TestDataFactory.findTestData("Data Files/E2E flow kennismedewerker - 28-02-2018")
String URL = data.getValue(1, 1)
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'Enter valid credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'Go to Hoodfpagina'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Hoofdpagina")
'checkpoint to check whether the element is clickable or not clickable'
CustomKeywords.'com.helper.validation.validationNverification.verifyElementNotClikable'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/Hoodfpagina/txtActualTopicDescription'))
'Go to QueueManagement'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Queue Management")
'count the number of cases in the queue'
CustomKeywords.'com.helper.validation.validationNverification.countElements'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/countElements'))
'go to kennis'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Kennis")
'click on the 1st kennis'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead2ndElementInList'()
'click on edit'
CustomKeywords.'page_kennis_salesforce.headerTab.clickOnBewerken'()
'confirm edit option to proceed'
CustomKeywords.'page_kennis_salesforce.headerTab.confirmBewerken'()
'move to element lightning-icon'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_kennis_salesforce/verantwoordelijkTooltip'))
'verify text'
CustomKeywords.'com.helper.validation.validationNverification.verifyTextEqual'(findTestObject('Object Repository/OR_salesForce/page_kennis_salesforce/txtVerantwoordelijkTooltip'),"De eigenaar kan geselecteerd worden via het 'verander eigenaar' scherm.")








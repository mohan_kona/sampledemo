import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'retrieve data from excel sheet'
def data = TestDataFactory.findTestData("Data Files/E2E flow kennismedewerker - 28-02-2018")
String URL = data.getValue(1, 1)
String uName = data.getValue(2, 1)
String pwd= data.getValue(3, 1)
'navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'Enter valid credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'go to rapporten'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Rapporten")
'click on "all mappen" under mappen'
CustomKeywords.'page_rapporten_salesForce.reportsDetails.mappenList'('Alle mappen')
'select "KS Support" from the list'
CustomKeywords.'page_rapporten_salesForce.allReportsBody.selectReportWithName'('KS Support')
'select subreport "Cases per Channel per CSJ (AH NL)"'
CustomKeywords.'page_rapporten_salesForce.allReportsBody.selectSubReportWithName'('Cases per Channel per CSJ (AH NL)')
'verify the number of rows'
String row_count = CustomKeywords.'com.helper.validation.validationNverification.countElements'(findTestObject('Object Repository/OR_salesForce/page_Rapporten_salesForce/allReportsBody/countInChannelperCSJ'))
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_Rapporten_salesForce/allReportsBody/countInChannelperCSJ'), row_count)



import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'extracting data from excel sheet by giving the excel path'
def data = TestDataFactory.findTestData('Data Files/createNewContact')
'URL will be taken from cell(1,1)'
String URL = data.getValue(1, 1)
'Username will be taken from cell(2,1)'
String uName = data.getValue(2, 1)
'Password will be taken from cell(3,1)'
String pwd = data.getValue(3, 1)
'Testuser value will be taken from cell(4,1)'
String testUser = data.getValue(4, 1)
'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(URL)
'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on search bar present on header'
CustomKeywords.'page_header_salesForce.headerUtilities.clickOnSearchBar'()
'searching a particular testuser by sending testuser name to the search bar to search'
CustomKeywords.'page_header_salesForce.headerUtilities.searchDrpdwn'(testUser)
'click on button "Gebruikersgegevens"'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickGebruikersgegevens'()
'click on "Inloggen" button to ensure successfull login of the test user'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickInloggen'()
'click on "casetype dropdown" and then click on "Contactpersonen" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Contactpersonen")
'click on any of the account'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'click on telefoon button top send text'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnTelefoon'))
'send an invalid telefoon number'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/txtTelefoon'), "0612345679")
'click on save'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnsave'))
'verify for warning text'
WebUI.verifyElementText(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/invalidTelefoonTxt'), 'Het ingevoerde telefoonnummer formaat is ongeldig. Een telefoonnummer moet altijd beginnen met "+" gevolgd door de landcode en een geldig telefoonnummer.')
'undo to give valid text'
CustomKeywords.'page_contactPersons_salesForce.DetailsTab.clickOnUndo'()
'send an valid telefoon number'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/txtTelefoon'), "+31612345679")
'click on save'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/DetailsTab/btnsave'))

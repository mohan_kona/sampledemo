import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_salesforce.navigateAndEnteringUserCredentials.navigateToURL'(AppURL)

'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)

'click in kennis'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(anotherSalesForceSection)

'click on "case activities" dropdown'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnRecentActivtiesDropDown'()

'click on "recent activities" present in the case activities dropdown'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.selectSubTypeInDropDown'(AnotherSubCategory_SFSection)

'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()

'click on link symbol present in the page'
CustomKeywords.'page_complaintCaseCollection_salesForce.KnowledgeMaking.clickOnLinkSymbol'()

'send specified text to text area'
CustomKeywords.'page_complaintCaseCollection_salesForce.KnowledgeMaking.sendTextToLinkTab'()

'click on save to save the link'
CustomKeywords.'page_complaintCaseCollection_salesForce.KnowledgeMaking.saveLink'()




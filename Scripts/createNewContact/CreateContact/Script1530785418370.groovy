import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(AppURL)

'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)

'login with testUser'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.loginByAnotherUser'(testUser)

'click on "casetype dropdown" and then click on "Contactpersonen" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(salesForce_section)

'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()

'select record type'
CustomKeywords.'page_contactPersons_salesForce.page_createContact.selectContactType'(contactType)

'Click on "to next"'
CustomKeywords.'page_contactPersons_salesForce.page_createContact.clickOnVolgende'()

'enter First name'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/txtVoornaam'), 
    fName)

'enter last name'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/txtAchternaam'), 
    lName)

'enter email'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/txtEmail'), 
    mail)

'perform TAB'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.performTab'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/txtEmail'))

'save the details of the customer'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/btnOpslaan'))

WebUI.delay(3)

CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/btnOpslaan'))

'close all tabs'
CustomKeywords.'page_header_salesForce.headerUtilities.closeAllTabs'()

'log out from the current user'
CustomKeywords.'page_header_salesForce.headerUtilities.clickLogOut'()


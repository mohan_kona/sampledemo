import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'navigate to the particular URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(AppURL)
'enter valid user credentials'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'click on cases'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(salesForceSection)
'click on "case type" dropdown'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnRecentActivtiesDropDown'()
'click On AH KS Teamleiders'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.selectSubTypeInDropDown'(subCategory_SFSection)
//CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.selectSubTypeInDropDown'(subCategory_SFSection)
'click on one of the cases and store it into a string'
String caseNum=CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'click on Feed tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_feed'))
'click On Email'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/clickEMail'))
'scroll to send button'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/btnToSend'))
'click on send mail '
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/btnToSend'))
'click on Oplossen in tab'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.clickOnCurrentStep'(currentStep)
'click on Huidige stap aanduiden als voltooid'
CustomKeywords.'page_caseInfo_salesForce.header_CurrentStep.markAsCurrentStep'()
'move to all updates'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_allUpdates'))
'click on Opmerking tab to enable it'
//CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabOpmerking/IconOpmerking'))
//CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabOpmerking/IconOpmerking'))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabOpmerking/Icon_Plaatsnotitie'))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabOpmerking/Icon_Plaatsnotitie'))
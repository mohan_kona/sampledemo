import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable



'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'("https://test.salesforce.com")
'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'("bharat.jambhorkar@ahold.com.uat", "butterfly@2985")
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(salesForceSection)
'click on new button'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton'()
'select record type'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.selectRecordType'()
'Click on "to next"'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.clickOnVolgende'()
'move to element'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/ObjAanvullendeGegevens'))
'provide case data'
List<WebElement> caseData = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/caseFieldInfoElements'),10)
println caseData.size()
'change klant(customer) name'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.sendCaseDetails'(caseData, custName, "Onbekend")
'change contact person'
CustomKeywords.'page_complaintCaseCollection_salesForce.createACase.sendCaseDetails'(caseData, contactPerson, "Onbekend")
'move to additional info tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/objWebgegevens'))
'add additional info'
List<WebElement> additionalData = WebUiBuiltInKeywords.findWebElements(findTestObject('OR_salesForce/page_complaintCaseCollection_salesForce/createACase/AdditionalCasefieldInfoElements'),10)
'change banner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(0))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'(banner)
'change Customer Service Journey'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(1))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'(CSJ)
'change status'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(2))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'(status)
'change categorie'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(3))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'(categorie)
'change case origin'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(additionalData.get(4))
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown'(caseOrigin)
'click on save button'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
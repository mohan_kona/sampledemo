import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

'Navigate to URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(AppURL)

'enter user details'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)

'click On Cases'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(salesForceSection)

'click on "case type" dropdown'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnRecentActivtiesDropDown'()

'click On AH KS Teamleiders'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.selectSubTypeInDropDown'(SubCategory_SFSection)

'click on one of the cases and store it into a string'
String caseNum = CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()

'click on details'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/Icon_details'))

'click on Gerelateerde Cases'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabGerelateerd/Icon_Gerelateerd'))

'click on Feed tab'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_feed'))

//'click on Opmerking tab to enable it'
//CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabOpmerking/IconOpmerking'))
'click on Plaatsnotitie tab to enable it'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabOpmerking/Icon_Plaatsnotitie'))

'click on text area in Plaat notitie'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabOpmerking/btnOpmerkingTextarea'))

'send specified text to textarea'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabOpmerking/textOpmerkingTextarea'), 'Verified')

'click on save button'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabOpmerking/btnSave'))

'clicks on E-mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/clickEMail'))

'scroll to send button'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/btnToSend'))

'clicks on send mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/btnToSend'))

'disable left tab containing "CSJ"'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.comboBox'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/disableCSJ'), 'Customer Service Journey')

'click on "Customer service journey" on left tab'
WebDriver driver = DriverFactory.getWebDriver()

List<WebElement> leftTabEle = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/CSJinfoFields'),10)

println(leftTabEle.size())

CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(leftTabEle.get(0))

'change "customer service journey" details'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(CSJ)

'click on status button'
List<WebElement> leftTabElement = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/bannerInfoFields'),10)

CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorElement'(leftTabElement.get(1))

'change status'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(status)

'save the given details'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/btnSave'))


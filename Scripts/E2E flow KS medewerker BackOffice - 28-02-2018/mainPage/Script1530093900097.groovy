import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import feedBackPage.feedBackObjects as feedBackObjects
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(AppURL)

'This method will enter username and password and clicks on login'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)

'login with testUser'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.loginByAnotherUser'(testUser)

'click on "Hoofdpagina" tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(salesForceSection)

'click on omni-channel'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.clickOmniChannel'()

'click on drop down to change status'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.tiggerOmnichannelDrpdwn'()

'set Status to Online'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.setOmnichannelStatus'(OmniChannelStatus)

'click on "details" icon'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/Icon_details'))

'clicks on E-mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/clickEMail'))

'scroll to send button'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/btnToSend'), 10)

'clicks on send mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/btnToSend'))

'move to all updates'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.moveToElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_allUpdates'))

'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(another_salesForceSection)

//'click on start again to minimise it'
//CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickStart'()
'click on "case activities" dropdown'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnRecentActivtiesDropDown'()

//'click on AH KS BackOffice'
//CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.selectSubTypeInDropDown'(subCategory_SFSection)

'click on Hoofdpagina'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickTab'(salesForceSection)


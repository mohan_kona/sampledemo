import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.eclipse.persistence.internal.oxm.record.json.JSONParser.value_return
import org.junit.After
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.interactions.Actions

'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(AppURL)
'This method will enter username and password and clicks on login\r\n'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'login with testUser'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.loginByAnotherUser'(testUser)
'click on "casetype dropdown" and then click on "cases" present in the dropdown'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'(salesForceSection)
'click on "case activities" dropdown'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnRecentActivtiesDropDown'()
'clicks on my managed cases cases'
CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.selectSubTypeInDropDown'(subCategory_SFSection)
'clicks on 1st test case'
String caseNum=CustomKeywords.'page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList'()
'click on omni-channel'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.clickOmniChannel'()
'click on drop down to change status'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.tiggerOmnichannelDrpdwn'()
'set Status to Online'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.setOmnichannelStatus'(OmniChannelStatus)
'check cases in mijn werk'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.clickPushedCaseIn'(pushedCasesIn)
WebUI.delay(10)
'click on omni-channel'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.clickOmniChannel'()
WebUI.delay(10)
//add steps
'click on "feed" icon'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/Icon_feed'))
'click Email'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/clickEMail'))
'click on change owner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnChangeOwner'))
'send owner name to text area'
CustomKeywords.'page_complaintCaseCollection_salesForce.ChangeOwner.sendOwnerName'(caseOwnerName)
'click on submit button'
CustomKeywords.'com.helper.CommonFunctions.commonFunctions.javaScriptExecutor'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnSubmit'))
'minimise start'
CustomKeywords.'page_testUser_salesForce.testUserHomePage.clickStart'()
'close child tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.closeTab'()
'click on tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.clickTab'(caseNum)
'click  on article'
CustomKeywords.'page_caseInfo_salesForce.miscSection.clickArticle'()
'click on one of the article'
CustomKeywords.'page_caseInfo_salesForce.miscSection.clickOnArticleLink'()
'close child tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.closeTab'()
'click on details'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/Icon_details'))
'disable button'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.comboBox'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/disableCSJ'),'Customer Service Journey')
'click on customer service journey'
List<WebElement> categorieDrpDwn = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabDetails/CSJinfoFields'),10)
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSEXecutorElement'(categorieDrpDwn.get(0))
'change customer service journey'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(CSJ_case1)
'click Category'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSEXecutorElement'(categorieDrpDwn.get(1))
'change category'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(category_case1)
'click subCategory'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSEXecutorElement'(categorieDrpDwn.get(2))
'change subcategory'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(subCategory_case1)
'click on save button'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
'click on change owner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnChangeOwner'))
'send owner name to text area'
CustomKeywords.'page_complaintCaseCollection_salesForce.ChangeOwner.sendOwnerName'(anotherCaseOwner)
'click on submit button'
CustomKeywords.'com.helper.CommonFunctions.commonFunctions.javaScriptExecutor'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnSubmit'))
'disable left tab'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.comboBox'(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/disableCSJ'),'Customer Service Journey')
'click on Customer service journey on left tab'
List<WebElement> leftTabEle = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/caseDetailsLeftTab/CSJinfoFields'))
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSEXecutorElement'(leftTabEle.get(0))
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(CSJ_case2)
'click Category'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSEXecutorElement'(leftTabEle.get(1))
'change category'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(category_case2)
'click subCategory'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSEXecutorElement'(leftTabEle.get(2))
'change subcategory'
CustomKeywords.'page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown'(subCategory_case2)
'click save'
CustomKeywords.'page_caseInfo_salesForce.completeInfoCase.saveCase'()
'click on change owner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnChangeOwner'))
'send owner name to text area'
CustomKeywords.'page_complaintCaseCollection_salesForce.ChangeOwner.sendOwnerName'(caseOwnerName)
'click on submit button'
CustomKeywords.'com.helper.CommonFunctions.commonFunctions.javaScriptExecutor'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnSubmit'))
'clicks on E-mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/completeInfoCase/TabFeed/clickEMail'))
'clicks on send mail'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/completeInfoCase/TabFeed/btnToSend'))
'click on search bar in knowledge'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.clickSearchBarKnowledge'()
'send text to search bar'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.sendTextSearchBarKnowledge'(knowledgeValue)
'click on value in dropdown'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.clickOnKnowledgeDrpDwn'()
'double click on velden'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.clickOnVelden'()
'close child tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.closeTab'()
'click on search bar in knowledge'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.clickSearchBarKnowledge'()
'send text to search bar'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.sendTextSearchBarKnowledge'(anotherKnowledgeValue) 
'click on value in dropdown'
CustomKeywords.'page_caseInfo_salesForce.KnowledgeTab.clickOnKnowledgeDrpDwn'()
'close child tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.closeTab'()


















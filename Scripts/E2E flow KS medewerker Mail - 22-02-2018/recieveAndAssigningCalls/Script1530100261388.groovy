import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


'This method will navigate to the corresponding URL'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'(AppURL)
'This method will enter username and password and clicks on login'
CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'(uName, pwd)
'login with testUser'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.loginByAnotherUser'(testUser)
'click on "Hoofdpagina" tab'
CustomKeywords.'page_consoleTabs_salesForce.consoleTabs.salesForceSectionList'("Hoofdpagina")
'click on omni-channel'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.clickOmniChannel'()
'click on drop down to change status'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.tiggerOmnichannelDrpdwn'()
'set Status to Online'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.setOmnichannelStatus'(OmniChannelStatus)
'check cases in mijn werk'
CustomKeywords.'page_bottomUtilityBar.omniChannelBar.clickPushedCaseIn'(pushedCaseIn)
'change the name of the owner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.clickVisibleElement'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnChangeOwner'))
'change owner type'
CustomKeywords.'page_complaintCaseCollection_salesForce.ChangeOwner.clickOwnerWachtrijen'()
'name the owner of the case'
CustomKeywords.'page_complaintCaseCollection_salesForce.ChangeOwner.sendText'(ownerName)
'click on submit to assign the particular case to the owner'
CustomKeywords.'com.helper.CommonFunctions.functionLibrary.JSExecutorObject'(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/changeOwner/btnSubmit'))


import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

class NewTestListener {
	
	@BeforeTestSuite
	def sampleBeforeTestSuite() 
	{
//		'extracting data from excel sheet by giving the excel path'
//		def data = TestDataFactory.findTestData('Data Files/E2E flow kennismedewerker - 21-02-2018')
//		'URL will be taken from cell(1,1)'
//		String URL = data.getValue(1, 1)
//		'Username will be taken from cell(2,1)'
//		String uName = data.getValue(2, 1)
//		'Password will be taken from cell(3,1)'
//		String pwd = data.getValue(3, 1)
//		'This method will navigate to the corresponding URL'
//		CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL'("http://test.salesforce.com/")
//		'This method will enter username and password and clicks on login\r\n'
//		CustomKeywords.'page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails'("bharat.jambhorkar@ahold.com.uat", "butterfly@2985")
	}
	
	@AfterTestCase
	def sampleAfterTestCase() 
	{
//		'closes all the opened tabs'
//		CustomKeywords.'page_header_salesForce.headerUtilities.closeAllTabs'()
	}

	
	
}
package page_caseInfo_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class caseDetailsLeftTab 
{
	WebDriver driver = DriverFactory.getWebDriver()
	Actions act1 = new Actions(driver)
	@Keyword
	public comboBox(TestObject path,String name)
	{
		WebUI.delay(5)
		
		List<WebElement> gegevensList = WebUiBuiltInKeywords.findWebElements(path,10);
		println gegevensList.size()
//		act1.moveToElement(gegevensList.get(gegevensList.size()-1)).build().perform()
		for (int i=0;i<gegevensList.size()-1;i++)
		{
			for(int j=1;j<=2;j++)
			{
			String value =gegevensList.get(i).findElement(By.xpath(".//div["+j+"]/div/div[1]/span")).getText()
			if(value.equals(name))
			{
				println value
				act1.moveToElement(gegevensList.get(gegevensList.size()-1)).build().perform()
				WebElement element = gegevensList.get(i).findElement(By.xpath(".//div[" + j + "]/div/div[1]/span//preceding::div[1]//button"))
				JavascriptExecutor executor = ((driver)as JavascriptExecutor)
				executor.executeScript("arguments[0].click()", element)
				
				}
			}
		}
		WebUI.delay(5)
	}
	
	@Keyword
	public selectValueFromDropdown(String txtDropDown)
	{
		List<WebElement> Dropdown = driver.findElements(By.xpath("//div[@class='select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short visible positioned']//div[@class='select-options']/ul/li"))
		for(int i = 0;i<=Dropdown.size()-1;i++)
		{
			String valueDropdown = Dropdown.get(i).findElement(By.xpath(".//a")).getText()
			if(valueDropdown.equalsIgnoreCase(txtDropDown)) {
			Dropdown.get(i).findElement(By.xpath(".//a")).click()
			}
		}
	}

}

package page_caseInfo_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class header_CurrentStep 
{
	@Keyword
	public clickOnCurrentStep(String StepName)
	{
		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> steps = driver.findElements(By.xpath("//div[@class='cd-header header']//div[@class='scroller stretch']//li"))
		for(int i=0;i<=steps.size()-1;i++)
		{
			String currentStep = steps.get(i).findElement(By.xpath('.//a/span[2]')).getText()
			if(currentStep.equalsIgnoreCase(StepName))
			{
				WebElement element =steps.get(i).findElement(By.xpath('.//a'))
				JavascriptExecutor executor = ((driver)as JavascriptExecutor)
				executor.executeScript("arguments[0].click()", element)
				WebUI.delay(5)
			}
		}
		
		
	}
	
	@Keyword
	public markAsCurrentStep()
	{
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/header_CurrentStep/markAsCurrentStep'), 10)
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/header_CurrentStep/markAsCurrentStep'))
	}

}

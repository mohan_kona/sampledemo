package page_caseInfo_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.server.DefaultDriverFactory

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class completeInfoCase 
{
	WebDriver driver = DriverFactory.getWebDriver()
	
	@Keyword
	public clickOnNewButton()
	{
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabGerelateerd/btnNew'))
	}
	
	@Keyword
	public setTextToField(String ownerName)
	{
		WebUI.delay(5)
		WebUI.sendKeys(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabGerelateerd/sendTextToTextArea'), ownerName)
	}
	
	@Keyword
	public saveCase()
	{
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabGerelateerd/saveCase'))
		WebUI.delay(5)
	}
	
	@Keyword
	public sendReceptionist(String receiptionistName)
	{
		WebUI.sendKeys(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/sendReceptionist'), receiptionistName)
		WebUI.delay(5)
	}

	
	@Keyword
	public emailBodyImage() {
		WebUI.delay(10)
		WebUI.switchToFrame(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/emailMainFrame'), 10)
		WebUI.switchToFrame(findTestObject('OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/TabEmail/emailFrame'), 10)
		WebUI.scrollToElement(findTestObject('Object Repository/OR_salesForce/page_caseInfo_salesForce/completeInfoCase/TabFeed/emailBodyImage'), 20)
		WebUI.switchToDefaultContent()
	}

	@Keyword
	public selectValueFromDropdown(String txtDropDown)
	{
		List<WebElement> Dropdown = driver.findElements(By.xpath("//div[@class='select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short visible positioned']//div[@class='select-options']/ul/li"))
		for(int i = 0;i<=Dropdown.size()-1;i++)
		{
			String valueDropdown = Dropdown.get(i).findElement(By.xpath(".//a")).getText()
			println valueDropdown
			if(valueDropdown.equalsIgnoreCase(txtDropDown)) {
			Dropdown.get(i).findElement(By.xpath(".//a")).click()
			}
		}
	}

}

package page_CustomerInfo_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class mergeDuplicateCase 
{
	

	
	@Keyword
	public checkBoxDuplicateCase()
	{
		WebUI.delay(5)
//		WebUI.click(findTestObject('OR_salesForce/page_CustomerInfo_salesForce/DuplicatesTab/mergeDuplicateCase/checkBoxDuplicateCase'))
		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> allTabs = driver.findElements(By.xpath('//div[@class="listViewContent"]//tbody/tr'))
		println allTabs.size()
		for(int i=1;i<=allTabs.size()-1;i++)
		{
			allTabs.get(i).findElement(By.xpath(".//td[1]/span")).click()
		}
		
	}
	
	@Keyword
	public toNextButton()
	{
		WebUI.delay(5)
		WebUI.click(findTestObject('OR_salesForce/page_CustomerInfo_salesForce/DuplicatesTab/mergeDuplicateCase/toNextButton'))
	}
	
	@Keyword
	public clickOnCustomerRecordType(String recordType)
	{
		WebDriver driver = DriverFactory.getWebDriver()
		WebUI.delay(5)
		List<WebElement> customerRecordType= driver.findElements(By.xpath("//*[@id='split-left']//tbody/tr"))
		for(int i=1;i<=customerRecordType;i++)
		{
			String type=customerRecordType.get(i).findElement(By.xpath("//td[6]/span/span//ancestor::tbody/tr["+i+"]/td[6]/span/span")).getText()
			println type
			if(type.equalsIgnoreCase(recordType))
			{
				customerRecordType.get(i).findElement(By.xpath("/th//ancestor::tbody/tr["+i+"]/th")).click()
				break
			}
		}
	}

}

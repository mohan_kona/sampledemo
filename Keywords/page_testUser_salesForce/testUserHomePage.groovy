package page_testUser_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class testUserHomePage 
{
	@Keyword
	public clickGebruikersgegevens()
	{
		if(WebUI.verifyElementClickable(findTestObject('Object Repository/OR_salesForce/page_testUser_salesForce/buttonGebruikersgegevens'))==true)
		{
			WebUI.click(findTestObject('Object Repository/OR_salesForce/page_testUser_salesForce/buttonGebruikersgegevens'))
			WebUI.delay(5)
		}
	}
	
	@Keyword
	public clickInloggen()
	{
		WebUI.delay(5)
		WebUI.verifyElementPresent(findTestObject('Object Repository/OR_salesForce/page_testUser_salesForce/frameObj'),20)
		WebUI.switchToFrame(findTestObject('Object Repository/OR_salesForce/page_testUser_salesForce/frameObj'),20)
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_testUser_salesForce/btnLogin'))
		WebUI.delay(5)
	}
	
	@Keyword
	public clickStart()
	{
		WebUI.delay(5)
		WebUI.click(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/btnStart'))
		WebUI.delay(5)
	}
	
	@Keyword
	public enterStationNumber(String stationNumber)
	{
		WebUI.delay(15)
		WebUI.switchToFrame(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/startFrame'), 10)
		WebUI.waitForElementVisible(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/TextStationNumber'), 60)
		WebUI.doubleClick(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/TextStationNumber'))
		WebUI.sendKeys(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/TextStationNumber'), stationNumber)
		WebUI.click(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/enterStationNumber'))
		WebUI.switchToDefaultContent()

	}
	
	@Keyword
	public clickOnbenkend()
	{
		WebUI.delay(10)
		WebUI.switchToFrame(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/startFrame'), 10)
		WebUI.delay(5)
		WebUI.click(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/caseAccount(start)'))
		WebUI.switchToDefaultContent()
		WebUI.delay(5)
	}
	
	@Keyword
	public clickCaseObject()
	{
		WebUI.delay(5)
		WebUI.switchToFrame(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/startFrame'), 10)
		WebUI.delay(5)
		WebUI.click(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/caseObject(start)'))
		WebUI.switchToDefaultContent()
		WebUI.delay(5)
	}
	
	@Keyword
	public clickBtnReply()
	{
		WebUI.delay(5)
		WebUI.switchToFrame(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/startFrame'), 10)
		WebUI.delay(5)
		WebUI.click(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/btnReply'))
		WebUI.switchToDefaultContent()
		WebUI.delay(5)
	}
	
	
	@Keyword
	public String clickCase1()
	{
		WebUI.delay(5)
		String case1=WebUI.getText(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/select1stCase(onbenkend)'))
		WebUI.click(findTestObject('OR_salesForce/page_testUser_salesForce/bottomUtlityBar/select1stCase(onbenkend)'))
		return case1
	}
}

package page_header_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable
import page_consoleTabs_salesForce.consoleTabs
import page_testUser_salesForce.testUserHomePage
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class headerUtilities extends testUserHomePage {

	WebDriver driver = DriverFactory.getWebDriver()

	@Keyword
	public clickOnSearchBar() {
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_header_salesForce/searchBar'))
		WebUI.delay(5)
	}

	@Keyword
	public clickLogOut() {
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_header_salesForce/logOut'))
	}

	@Keyword
	public searchDrpdwn(String testUser) {
		WebUI.sendKeys(findTestObject('Object Repository/OR_salesForce/page_header_salesForce/searchBar'), testUser)
		WebUI.delay(5)

		List<WebElement> drpdwnlist = driver.findElements(By.xpath("//*[@id='oneHeader']//div[@class='listContent']//ul/li"))
		println drpdwnlist.size()
		for(int i=1;i<=drpdwnlist.size()-1;i++) {
			String e = drpdwnlist.get(i).findElement(By.xpath(".//a/div[2]/span")).getAttribute("title")
			println e
			if (e.equals(testUser)) {
				println "mohan"
				drpdwnlist.get(i).click();
			}
		}
	}

	@Keyword
	public void closeAllTabs() {
		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> allTabs = driver.findElements(By.xpath('//*[@id="oneHeader"]//div[@class="tabBar slds-grid"]/ul[2]/li'))
		println allTabs.size()
		for(int i=1;i<=allTabs.size()-1;i++) {
			allTabs.get(i).findElement(By.xpath(".//div[2]/button")).click()
		}
	}

	@Keyword
	public void sendTextToSearchbar(String userName) {
		WebUI.sendKeys(findTestObject('Object Repository/OR_salesForce/page_header_salesForce/searchBar'), userName)
		WebUI.sendKeys(findTestObject('Object Repository/OR_salesForce/page_header_salesForce/searchBar'),Keys.chord(Keys.ENTER))
	}

	@Keyword
	public void clickSubTabByName(String subTabName) {
		List<WebElement> subTab = driver.findElements(By.xpath("//div[@class='tabBarContainer']/div/div/ul[2]/li"))
		println subTab.size()
		for(int i = 1;i<subTab.size();i++) {
			String tabName = subTab.get(i).findElement(By.xpath(".//a/span")).getText()
			if(tabName.equalsIgnoreCase(subTabName)) {
				WebElement ele_SubTab= subTab.get(i).findElement(By.xpath(".//a"))
				JavascriptExecutor executor = ((driver)as JavascriptExecutor)
				executor.executeScript("arguments[0].click()", ele_SubTab)
			}
		}
	}
	@Keyword
	public void clickTabByName(String TabName) {
		List<WebElement> Tab = driver.findElements(By.xpath("//*[@id='oneHeader']//div[@class='tabBar slds-grid']/ul[2]/li"))
		println Tab.size()
		for(int i = 1;i<=Tab.size();i++) {
			String tabName = Tab.get(i).findElement(By.xpath(".//a/lightning-icon/span")).getText()
			if(tabName.equalsIgnoreCase(TabName)) {
				Tab.get(i).findElement(By.xpath(".//a")).click()
			}
		}
	}

	@Keyword
	public void closeTabByName(String TabName) {
		List<WebElement> Tab = driver.findElements(By.xpath("//*[@id='oneHeader']//div[@class='tabBar slds-grid']/ul[2]/li"))
		println Tab.size()
		for(int i = 1;i<=Tab.size();i++) {
			String tabName = Tab.get(i).findElement(By.xpath(".//a/span")).getText()
			if(tabName.equalsIgnoreCase(TabName)) {
				Tab.get(i).findElement(By.xpath(".//div[2]/button")).click()
			}
		}
	}
}

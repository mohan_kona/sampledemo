package page_rapporten_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject


import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class allReportsBody 
{
	WebDriver driver = DriverFactory.getWebDriver()
	Actions action = new Actions(driver)
	@Keyword
	public selectReportWithName(String report_name)
	{
		List<WebElement> reports_list = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_Rapporten_salesForce/allReportsBody/reportCollection'),10)
		for(int i=0;i<=reports_list;i++)
		{
			String report = reports_list.get(i).findElement(By.xpath('.//div[@class="slds-truncate"]/lightning-icon//following::a[1]')).getText()
			action.moveToElement(reports_list.get(i).findElement(By.xpath('.//div[@class="slds-truncate"]/lightning-icon//following::a[1]')))
			action.build().perform()
			if(report.equalsIgnoreCase(report_name))
			{
				reports_list.get(i).findElement(By.xpath('.//div[@class="slds-truncate"]/lightning-icon//following::a[1]')).click()
			}
		}
	}
	
	
	@Keyword
	public selectSubReportWithName(String report_name)
	{
			List<WebElement> reports_list = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_Rapporten_salesForce/allReportsBody/reportCollection'),10)
			for(int i=0;i<=reports_list;i++)
			{
				String report = reports_list.get(i).findElement(By.xpath('.//th//a')).getText()
				action.moveToElement(reports_list.get(i).findElement(By.xpath('.//th//a')))
				action.build().perform()
				if(report.equalsIgnoreCase(report_name))
				{
					reports_list.get(i).findElement(By.xpath('.//th')).click()
				}
			}
	}

}

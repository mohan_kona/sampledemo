package page_consoleTabs_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class consoleTabs 
{
		WebDriver driver = DriverFactory.getWebDriver()
		
		@Keyword
		public salesForceSectionList(String section_name)
		{
			WebUI.delay(5)
			WebUI.waitForElementClickable(findTestObject('Object Repository/OR_salesForce/page_consoleTabs_salesForce/dropDownConsole'), 10)
			WebUI.click(findTestObject('Object Repository/OR_salesForce/page_consoleTabs_salesForce/dropDownConsole'))
			List<WebElement> section_list = WebUiBuiltInKeywords.findWebElements(findTestObject('Object Repository/OR_salesForce/page_consoleTabs_salesForce/salesForceSectionList'),30)
			for(int i=0;i<=section_list.size()-1;i++)
			{
				String sectionNameInList = section_list.get(i).findElement(By.xpath(".//a/span")).getText() 
				if(sectionNameInList.equalsIgnoreCase(section_name))
				{
					section_list.get(i).findElement(By.xpath(".//a")).click()
				}
			}
		}
		
		
		@Keyword
		public clickMainTab()
		{
			WebUI.delay(5)
			WebUI.click(findTestObject('Object Repository/OR_salesForce/page_consoleTabs_salesForce/mainTab'))
			WebUI.delay(5)
		}
		
		@Keyword
		public clickTab(String caseNumber)
		{
			WebUI.delay(5)
			
			List<WebElement> tabList = driver.findElements(By.xpath('//*[@role="tab"]/lightning-icon//ancestor::a/span'))
			println tabList.size()
			for(int i=0;i<=tabList.size()-1;i++)
			{
				String tabName=tabList.get(i).getText()
				println tabName
				if(tabName.equals(caseNumber))
				{
					println tabName
	//				JavascriptExecutor executor = ((driver)as JavascriptExecutor)
	//				executor.executeScript("arguments[0].click()", tabList[i])
					tabList.get(i).click()
				}
			
			}
		}
		
		@Keyword
		public closeTab()
		{
			WebUI.delay(5)
			WebUI.click(findTestObject('Object Repository/OR_salesForce/page_consoleTabs_salesForce/closeTab'))
			WebUI.delay(5)
		}

}

package page_complaintCaseCollection_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.sun.org.apache.xalan.internal.xsltc.compiler.If

import internal.GlobalVariable as GlobalVariable


import MobileBuiltInKeywords as Mobile

import WebUiBuiltInKeywords as WebUI

public class createACase 
{
	
	WebDriver driver = DriverFactory.getWebDriver()
	
	 
	@Keyword
	public selectRecordType() {
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/RadioBtnCaseType'))
	} 
 
	@Keyword
	public clickOnVolgende()
	{
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/btnVolgende'), 10)
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/btnVolgende'))
		WebUI.delay(5)
	}
	
	@Keyword
	public sendCaseDetails(List<WebElement> caseData, String CaseInfo, String name)
	{
		if(CaseInfo.equalsIgnoreCase("Klantnaam"))
		{
			caseData.get(1).findElement(By.xpath("//div[2]//label/span[text()='Bedrijfs- of klantnaam (klantnaam)']//following::div[1]//div[@class='autocompleteWrapper slds-grow']")).click();
			caseData.get(1).findElement(By.xpath("//div[2]//label/span[text()='Bedrijfs- of klantnaam (klantnaam)']//following::div[1]//div[@class='autocompleteWrapper slds-grow']/input")).sendKeys(name);
			WebUI.delay(5)
			driver.findElement(By.xpath("//div[2]//label/span[text()='Bedrijfs- of klantnaam (klantnaam)']//following::div[1]//div[@class='autocompleteWrapper slds-grow']/div/div[2]/ul/li/a/div[2]/div[@title='"+name+"']")).click();
		} 
		else if(CaseInfo.equalsIgnoreCase("Contactpersoon"))
		{
			caseData.get(3).findElement(By.xpath("//div[2]//label/span[text()='Klant (contactpersoon)']//following::div[1]//div[@class='autocompleteWrapper slds-grow']")).click();
			caseData.get(3).findElement(By.xpath("//div[2]//label/span[text()='Klant (contactpersoon)']//following::div[1]//div[@class='autocompleteWrapper slds-grow']/input")).sendKeys(name);
			WebUI.delay(5)
			driver.findElement(By.xpath("//div[2]//label/span[text()='Klant (contactpersoon)']//following::div[1]//div[@class='autocompleteWrapper slds-grow']/div/div[2]/ul/li/a/div[2]/div[@title='"+name+"']")).click();						
		}
		else
		{
	
		}
	}
	
	@Keyword
	public caseTooltipNavigation(String text) {
		Actions action = new Actions(driver)
		action.moveToElement(driver.findElement(By.xpath("//span[text()='"+text+"']//following::div[1]")))
		action.build().perform()
	}
	
	@Keyword
	public selectValueFromDropdown(String txtDropDown) 
	{																
		List<WebElement> Dropdown = driver.findElements(By.xpath("//div[@class='select-options popupTargetContainer uiPopupTarget uiMenuList uiMenuList--default uiMenuList--left uiMenuList--short visible positioned']//div[@class='select-options']/ul/li"))
		for(int i = 0;i<=Dropdown.size()-1;i++) 
		{
			String valueDropdown = Dropdown.get(i).findElement(By.xpath(".//a")).getText()
			if(valueDropdown.equalsIgnoreCase(txtDropDown)) {
			Dropdown.get(i).findElement(By.xpath(".//a")).click()
			}
		}
	}
	@Keyword
	public winkelSearchDropdown(String userName) {
		WebUI.sendKeys(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/createACase/txtstoreWinkel'), userName)
		WebUI.delay(5)

		List<WebElement> drpdwnlist = driver.findElements(By.xpath("//span[text()='Winkel']//following::ul[@class='lookup__list  visible']/li"))
		println drpdwnlist.size()
		for(int i=1;i<=drpdwnlist.size()-1;i++) {
			String e = drpdwnlist.get(i).findElement(By.xpath(".//a/div[2]/div[1]")).getAttribute("title")
			if (e.equals(userName)) {
				drpdwnlist.get(i).click();
			}
		}
	}
 

		
}

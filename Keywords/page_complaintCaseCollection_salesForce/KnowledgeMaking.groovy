package page_complaintCaseCollection_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class KnowledgeMaking 
{
	WebDriver driver = DriverFactory.getWebDriver()
	@Keyword
	public clickOnLinkSymbol()
	{
		WebUI.delay(5)
		WebUI.waitForElementVisible(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/knowledgeMaking/frameKennis'), 10)
		WebUI.switchToFrame(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/knowledgeMaking/frameKennis'), 10)
		driver.findElement(By.xpath("//*[contains(@id,'cke') and @class='cke_toolbar'][5]//a[1]")).click()
		WebUI.switchToDefaultContent()
	}
	
	@Keyword
	public sendTextToLinkTab()
	{
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/knowledgeMaking/textAreaLink'))
		WebUI.delay(2)
		WebUI.sendKeys(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/knowledgeMaking/textAreaLink'), 'www.google.com')
	}
	
	@Keyword
	public saveLink()
	{
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/knowledgeMaking/BtnSaveLink'))
	}
	
	@Keyword
	public kennisTooltipNavigation(String text) {
		Actions action = new Actions(driver)
		action.moveToElement(driver.findElement(By.xpath("//span[text()='"+text+"']//following::div[1]")))
		action.build().perform()
	}

}

package page_complaintCaseCollection_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class allTypesCaseComplaints 
{
	WebDriver driver = DriverFactory.getWebDriver()
	
	@Keyword
	public clickOnRecentActivtiesDropDown()
	{
		WebUI.delay(5)
		WebUI.doubleClick(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/recentActDropDown'))
		WebUI.delay(5)
	}
	
	
	@Keyword
	public selectSubTypeInDropDown(String Sub_value)
	{
		List<WebElement> sub_Type = driver.findElements(By.xpath("//*[contains(@id,'virtualAutocompleteListbox')]//li"))
		for(int i=1;i<=sub_Type.size()-1;i++)
		{
			String value = sub_Type.get(i).findElement(By.xpath(".//a/span")).getText()
			if(value.equalsIgnoreCase(Sub_value))
			{
				sub_Type.get(i).findElement(By.xpath(".//a")).click()
				break
			}
		}
	}
	
	
	@Keyword
	public clickAndRead1stElementInList()
	{
		WebUI.verifyElementPresent(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/click1stElement'), 10)
		WebUI.delay(5)
		String caseNumber = WebUI.getText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/click1stElement'))
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/click1stElement'))
		WebUI.delay(5)
		return caseNumber
	}
	
	@Keyword
	public clickAndRead2ndElementInList()
	{
		WebUI.verifyElementPresent(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/click1stElement'), 10)
		WebUI.delay(5)
		String caseNumber = WebUI.getText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/click2ndElement'))
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/click2ndElement'))
		WebUI.delay(5)
		
	}
	
	@Keyword
	public clickAndRead3rdElementInList()
	{
		WebUI.delay(5)
		String title=WebUI.getText(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/recentActList'))
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/recentActList'))
		return title
		WebUI.delay(5)
	}
	
	@Keyword
	public clickOnNewButton()
	{
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_complaintCaseCollection_salesForce/btnNewKennis'))
	}
	
	@Keyword
	public clickOnNaamBasedOnRecordType(String recordName) {
		List<WebElement> ele = driver.findElements("//table[@data-aura-class='uiVirtualDataTable']/tbody/tr")
		for(int i = 0;i<=ele.size()-1;i++) {
			String recordType = ele.get(i).findElement(By.xpath(".//td[6]/span/span")).getText()
			if(recordType.equalsIgnoreCase(recordName)) {
				ele.get(i).findElement(By.xpath(".//th//a")).click()
			}
		}
	}
	

}

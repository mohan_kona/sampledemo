package page_contactPersons_salesForce

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class page_createContact {

	
	@Keyword
	public selectContactType(String contactType)
	{
		
		 if(contactType.equalsIgnoreCase("3e lijn support medewerker (intern)"))
		 {
			   TestObject radioBtn = WebUI.modifyObjectProperty(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/RadioBtnContactType'), 'xpath', 'equals', '//div[@class="changeRecordTypeLeftBody "]//legend//following::div[1]/label[1]/span[1]', false)
			   WebUI.click(radioBtn)
		 }
		 else if(contactType.equalsIgnoreCase("Contactpersoon"))
		 {
			 TestObject radioBtnContactPerson = WebUI.modifyObjectProperty(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/RadioBtnContactType'), 'xpath', 'equals', '//div[@class="changeRecordTypeLeftBody "]//legend//following::div[1]/label[2]/span[1]', true)
			 WebUI.click(radioBtnContactPerson)
		 }
		 else if(contactType.equalsIgnoreCase("Contactpersoon EMDA"))
		 {
			   TestObject radioBtnContactPersonEDMA = WebUI.modifyObjectProperty(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/RadioBtnContactType'), 'xpath', 'equals', '//div[@class="changeRecordTypeLeftBody "]//legend//following::div[1]/label[3]/span[1]', false)
			   WebUI.click(radioBtnContactPersonEDMA)
		 }
	}
	
	@Keyword
	public clickOnVolgende()
	{
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_contactPersons_salesForce/page_createContact/btnVolgende'))
		WebUI.delay(5)
	}

}

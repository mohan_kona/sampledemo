package com.helper.validation

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class validationNverification 
{
	WebDriver driver = DriverFactory.getWebDriver()
	
	@Keyword
	public verifyTextEqual(String actualxPath,  String expectedText)
	{
		List<WebElement> text_same = driver.findElements(By.xpath(actualxPath))
		for(int i=0;i<=text_same.size();i++)
		{
			String actualText = text_same.get(i).getText()
			if(WebUI.verifyEqual(actualText, expectedText)==true)
			println actualText+" and "+expectedText+" values are equal"
			else
			println actualText+" and "+expectedText+" values are not equal"
		}
	}
	
	@Keyword
	public verifyObjectEqual(Object actual, Object expected)
	{
		if(WebUI.verifyEqual(actual, expected)==true)
		println actual+" and "+expected+" values are equal"
		else
		println actual+" and "+expected+" values are not equal"
	}
	
	@Keyword
	public verifyElementNotClikable(TestObject obj)
	{
		if(WebUI.verifyElementClickable(obj)==true)
			println "element is clickable"
		else
			println "element not clickable"
	}
	
	@Keyword
	public int countElements(TestObject Obj_count)
	{
		List<WebElement> count = WebUiBuiltInKeywords.findWebElements(Obj_count,10)
		println "Number of elements are "+count.size()
		return count.size()
	}
	
}

package com.helper.CommonFunctions

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.SimpleDateFormat

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable
import page_header_salesForce.headerUtilities
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI


public class functionLibrary extends headerUtilities {
	WebDriver driver = DriverFactory.getWebDriver()

	@Keyword
	public void clickVisibleElement(TestObject ele_Object) {
		Thread.sleep(5000)
		List<WebElement> ele = WebUiCommonHelper.findWebElements(ele_Object, 10)
		println ele.size()
		if(ele.size()!=0) {
			for(int i=0;i<ele.size();i++){
				if(ele.get(i).isDisplayed()==true) {
					Boolean value = ele.get(i).isDisplayed()
					println value
					JSExecutorElement(ele.get(i))
				}
			}
		}
		WebUI.delay(5)
	}

	@Keyword
	public JSExecutorObject(TestObject obj) {
		WebUI.delay(5)
		WebElement element = WebUiCommonHelper.findWebElement(obj, 10)
		JavascriptExecutor executor = ((driver)as JavascriptExecutor)
		executor.executeScript("arguments[0].click()", element)
		WebUI.delay(5)
	}


	@Keyword
	public JSExecutorElement(WebElement element) {
		JavascriptExecutor executor = ((driver)as JavascriptExecutor)
		executor.executeScript("arguments[0].click()", element)
		WebUI.delay(5)
	}

	@Keyword
	public void sendTextToVisibleElement(TestObject Object_path,String text) {
		WebUI.delay(5)
		WebDriver driver= DriverFactory.getWebDriver()
		List<WebElement> ele = WebUiBuiltInKeywords.findWebElements(Object_path,30)
		println ele.size()
		if(ele.size()!=0) {
			for(int i=0;i<ele.size();i++){
				if(ele.get(i).isDisplayed()==true) {
					ele.get(i).sendKeys(text)
				}
			}
		}
	}

	@Keyword
	public moveToElement(TestObject path) {
		WebUI.delay(10)
		WebElement element = WebUiCommonHelper.findWebElement(path, 10)
		Actions action = new Actions(driver)
		action.moveToElement(element)
		action.build().perform()
	}

	@Keyword
	public performTab(TestObject path) {
		WebUI.delay(5)
		WebElement element = WebUiCommonHelper.findWebElement(path, 10)
		element.sendKeys(Keys.chord(Keys.TAB))
	}

	@Keyword
	public performEnter(TestObject path) {
		WebUI.delay(5)
		WebElement element = WebUiCommonHelper.findWebElement(path, 10)
		element.sendKeys(Keys.chord(Keys.ENTER))
	}


	@Keyword
	public static String currentDate(int nthDay ) {
		Date dt1 = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt1);
		c.add(Calendar.DATE, nthDay);
		dt1 = c.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("DD-MM-YYYY");
		String str = formatter.format(dt1);

		return str;
	}


	@Keyword
	public loginByAnotherUser(String testUser) {
		if(testUser!="") {
			clickOnSearchBar()
			searchDrpdwn(testUser)
			clickGebruikersgegevens()
			clickInloggen()
		}
	}
}

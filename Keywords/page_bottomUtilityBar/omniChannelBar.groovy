package page_bottomUtilityBar

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class omniChannelBar 
{
	WebDriver driver = DriverFactory.getWebDriver()
	
	@Keyword
	public clickOmniChannel()
	{
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_salesForce/page_bottomUtilityBar/btnOmniChannel'), 10)
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_bottomUtilityBar/btnOmniChannel'))
	}
	
	@Keyword
	public tiggerOmnichannelDrpdwn()
	{
		WebUI.waitForElementClickable(findTestObject('Object Repository/OR_salesForce/page_bottomUtilityBar/omniChannelBar/statusDrpDwn'), 10)
		WebUI.click(findTestObject('Object Repository/OR_salesForce/page_bottomUtilityBar/omniChannelBar/statusDrpDwn'))
	}
	

	@Keyword
	public setOmnichannelStatus(String status)
	{
		List<WebElement> setStatus = driver.findElements(By.xpath("//*[@title='Omni-Channel']//following::div[3]/div[1]//button//following-sibling::div//li"))
		for(int i=0;i<=setStatus.size()-1;i++)
		{
			String attribute = setStatus.get(i).getAttribute("title")
			if(attribute.equalsIgnoreCase(status))
			{
				setStatus.get(i).findElement(By.xpath(".//a")).click()
			}
			
		}
	}
	
	@Keyword
	public clickPushedCaseIn(String caseType)
	{
		if(caseType.equalsIgnoreCase("Nieuw"))
		{
			driver.findElement(By.xpath('//*[@title="Omni-Channel"]//following::div[@class="uiTabBar"]//li[1]/a')).click()
		}
		else if("Mijn werk")
		{
			driver.findElement(By.xpath('//*[@title="Omni-Channel"]//following::div[@class="uiTabBar"]//li[2]/a')).click()
		}
		
	}

}

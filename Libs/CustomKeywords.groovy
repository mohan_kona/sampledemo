
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import com.kms.katalon.core.testobject.TestObject

import org.openqa.selenium.WebElement

import java.util.List

import java.lang.Object


def static "page_header_salesForce.headerUtilities.clickOnSearchBar"() {
    (new page_header_salesForce.headerUtilities()).clickOnSearchBar()
}

def static "page_header_salesForce.headerUtilities.clickLogOut"() {
    (new page_header_salesForce.headerUtilities()).clickLogOut()
}

def static "page_header_salesForce.headerUtilities.searchDrpdwn"(
    	String testUser	) {
    (new page_header_salesForce.headerUtilities()).searchDrpdwn(
        	testUser)
}

def static "page_header_salesForce.headerUtilities.closeAllTabs"() {
    (new page_header_salesForce.headerUtilities()).closeAllTabs()
}

def static "page_header_salesForce.headerUtilities.sendTextToSearchbar"(
    	String userName	) {
    (new page_header_salesForce.headerUtilities()).sendTextToSearchbar(
        	userName)
}

def static "page_header_salesForce.headerUtilities.clickSubTabByName"(
    	String subTabName	) {
    (new page_header_salesForce.headerUtilities()).clickSubTabByName(
        	subTabName)
}

def static "page_header_salesForce.headerUtilities.clickTabByName"(
    	String TabName	) {
    (new page_header_salesForce.headerUtilities()).clickTabByName(
        	TabName)
}

def static "page_header_salesForce.headerUtilities.closeTabByName"(
    	String TabName	) {
    (new page_header_salesForce.headerUtilities()).closeTabByName(
        	TabName)
}

def static "com.helper.CommonFunctions.functionLibrary.clickVisibleElement"(
    	TestObject ele_Object	) {
    (new com.helper.CommonFunctions.functionLibrary()).clickVisibleElement(
        	ele_Object)
}

def static "com.helper.CommonFunctions.functionLibrary.JSExecutorObject"(
    	TestObject obj	) {
    (new com.helper.CommonFunctions.functionLibrary()).JSExecutorObject(
        	obj)
}

def static "com.helper.CommonFunctions.functionLibrary.JSExecutorElement"(
    	WebElement element	) {
    (new com.helper.CommonFunctions.functionLibrary()).JSExecutorElement(
        	element)
}

def static "com.helper.CommonFunctions.functionLibrary.sendTextToVisibleElement"(
    	TestObject Object_path	
     , 	String text	) {
    (new com.helper.CommonFunctions.functionLibrary()).sendTextToVisibleElement(
        	Object_path
         , 	text)
}

def static "com.helper.CommonFunctions.functionLibrary.moveToElement"(
    	TestObject path	) {
    (new com.helper.CommonFunctions.functionLibrary()).moveToElement(
        	path)
}

def static "com.helper.CommonFunctions.functionLibrary.performTab"(
    	TestObject path	) {
    (new com.helper.CommonFunctions.functionLibrary()).performTab(
        	path)
}

def static "com.helper.CommonFunctions.functionLibrary.performEnter"(
    	TestObject path	) {
    (new com.helper.CommonFunctions.functionLibrary()).performEnter(
        	path)
}

def static "com.helper.CommonFunctions.functionLibrary.currentDate"(
    	int nthDay	) {
    (new com.helper.CommonFunctions.functionLibrary()).currentDate(
        	nthDay)
}

def static "com.helper.CommonFunctions.functionLibrary.loginByAnotherUser"(
    	String testUser	) {
    (new com.helper.CommonFunctions.functionLibrary()).loginByAnotherUser(
        	testUser)
}

def static "page_CustomerInfo_salesForce.newCaseDialogBox.clickOnCaseherkomst"() {
    (new page_CustomerInfo_salesForce.newCaseDialogBox()).clickOnCaseherkomst()
}

def static "page_CustomerInfo_salesForce.newCaseDialogBox.clickOnStatus"() {
    (new page_CustomerInfo_salesForce.newCaseDialogBox()).clickOnStatus()
}

def static "page_CustomerInfo_salesForce.newCaseDialogBox.clickOnOpslaan"() {
    (new page_CustomerInfo_salesForce.newCaseDialogBox()).clickOnOpslaan()
}

def static "page_bottomUtilityBar.omniChannelBar.clickOmniChannel"() {
    (new page_bottomUtilityBar.omniChannelBar()).clickOmniChannel()
}

def static "page_bottomUtilityBar.omniChannelBar.tiggerOmnichannelDrpdwn"() {
    (new page_bottomUtilityBar.omniChannelBar()).tiggerOmnichannelDrpdwn()
}

def static "page_bottomUtilityBar.omniChannelBar.setOmnichannelStatus"(
    	String status	) {
    (new page_bottomUtilityBar.omniChannelBar()).setOmnichannelStatus(
        	status)
}

def static "page_bottomUtilityBar.omniChannelBar.clickPushedCaseIn"(
    	String caseType	) {
    (new page_bottomUtilityBar.omniChannelBar()).clickPushedCaseIn(
        	caseType)
}

def static "page_caseInfo_salesForce.completeInfoCase.clickOnNewButton"() {
    (new page_caseInfo_salesForce.completeInfoCase()).clickOnNewButton()
}

def static "page_caseInfo_salesForce.completeInfoCase.setTextToField"(
    	String ownerName	) {
    (new page_caseInfo_salesForce.completeInfoCase()).setTextToField(
        	ownerName)
}

def static "page_caseInfo_salesForce.completeInfoCase.saveCase"() {
    (new page_caseInfo_salesForce.completeInfoCase()).saveCase()
}

def static "page_caseInfo_salesForce.completeInfoCase.sendReceptionist"(
    	String receiptionistName	) {
    (new page_caseInfo_salesForce.completeInfoCase()).sendReceptionist(
        	receiptionistName)
}

def static "page_caseInfo_salesForce.completeInfoCase.emailBodyImage"() {
    (new page_caseInfo_salesForce.completeInfoCase()).emailBodyImage()
}

def static "page_caseInfo_salesForce.completeInfoCase.selectValueFromDropdown"(
    	String txtDropDown	) {
    (new page_caseInfo_salesForce.completeInfoCase()).selectValueFromDropdown(
        	txtDropDown)
}

def static "page_CustomerInfo_salesForce.mergeDuplicateCase.checkBoxDuplicateCase"() {
    (new page_CustomerInfo_salesForce.mergeDuplicateCase()).checkBoxDuplicateCase()
}

def static "page_CustomerInfo_salesForce.mergeDuplicateCase.toNextButton"() {
    (new page_CustomerInfo_salesForce.mergeDuplicateCase()).toNextButton()
}

def static "page_CustomerInfo_salesForce.mergeDuplicateCase.clickOnCustomerRecordType"(
    	String recordType	) {
    (new page_CustomerInfo_salesForce.mergeDuplicateCase()).clickOnCustomerRecordType(
        	recordType)
}

def static "page_testUser_salesForce.testUserHomePage.clickGebruikersgegevens"() {
    (new page_testUser_salesForce.testUserHomePage()).clickGebruikersgegevens()
}

def static "page_testUser_salesForce.testUserHomePage.clickInloggen"() {
    (new page_testUser_salesForce.testUserHomePage()).clickInloggen()
}

def static "page_testUser_salesForce.testUserHomePage.clickStart"() {
    (new page_testUser_salesForce.testUserHomePage()).clickStart()
}

def static "page_testUser_salesForce.testUserHomePage.enterStationNumber"(
    	String stationNumber	) {
    (new page_testUser_salesForce.testUserHomePage()).enterStationNumber(
        	stationNumber)
}

def static "page_testUser_salesForce.testUserHomePage.clickOnbenkend"() {
    (new page_testUser_salesForce.testUserHomePage()).clickOnbenkend()
}

def static "page_testUser_salesForce.testUserHomePage.clickCaseObject"() {
    (new page_testUser_salesForce.testUserHomePage()).clickCaseObject()
}

def static "page_testUser_salesForce.testUserHomePage.clickBtnReply"() {
    (new page_testUser_salesForce.testUserHomePage()).clickBtnReply()
}

def static "page_testUser_salesForce.testUserHomePage.clickCase1"() {
    (new page_testUser_salesForce.testUserHomePage()).clickCase1()
}

def static "page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnRecentActivtiesDropDown"() {
    (new page_complaintCaseCollection_salesForce.allTypesCaseComplaints()).clickOnRecentActivtiesDropDown()
}

def static "page_complaintCaseCollection_salesForce.allTypesCaseComplaints.selectSubTypeInDropDown"(
    	String Sub_value	) {
    (new page_complaintCaseCollection_salesForce.allTypesCaseComplaints()).selectSubTypeInDropDown(
        	Sub_value)
}

def static "page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead1stElementInList"() {
    (new page_complaintCaseCollection_salesForce.allTypesCaseComplaints()).clickAndRead1stElementInList()
}

def static "page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead2ndElementInList"() {
    (new page_complaintCaseCollection_salesForce.allTypesCaseComplaints()).clickAndRead2ndElementInList()
}

def static "page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickAndRead3rdElementInList"() {
    (new page_complaintCaseCollection_salesForce.allTypesCaseComplaints()).clickAndRead3rdElementInList()
}

def static "page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNewButton"() {
    (new page_complaintCaseCollection_salesForce.allTypesCaseComplaints()).clickOnNewButton()
}

def static "page_complaintCaseCollection_salesForce.allTypesCaseComplaints.clickOnNaamBasedOnRecordType"(
    	String recordName	) {
    (new page_complaintCaseCollection_salesForce.allTypesCaseComplaints()).clickOnNaamBasedOnRecordType(
        	recordName)
}

def static "page_complaintCaseCollection_salesForce.QueueManagement.btnRefresh"() {
    (new page_complaintCaseCollection_salesForce.QueueManagement()).btnRefresh()
}

def static "page_contactPersons_salesForce.DetailsTab.clickOnUndo"() {
    (new page_contactPersons_salesForce.DetailsTab()).clickOnUndo()
}

def static "page_contactPersons_salesForce.page_case.clickonFirstCase"() {
    (new page_contactPersons_salesForce.page_case()).clickonFirstCase()
}

def static "page_caseInfo_salesForce.miscSection.clickArticle"() {
    (new page_caseInfo_salesForce.miscSection()).clickArticle()
}

def static "page_caseInfo_salesForce.miscSection.clickOnArticleLink"() {
    (new page_caseInfo_salesForce.miscSection()).clickOnArticleLink()
}

def static "page_complaintCaseCollection_salesForce.KnowledgeMaking.clickOnLinkSymbol"() {
    (new page_complaintCaseCollection_salesForce.KnowledgeMaking()).clickOnLinkSymbol()
}

def static "page_complaintCaseCollection_salesForce.KnowledgeMaking.sendTextToLinkTab"() {
    (new page_complaintCaseCollection_salesForce.KnowledgeMaking()).sendTextToLinkTab()
}

def static "page_complaintCaseCollection_salesForce.KnowledgeMaking.saveLink"() {
    (new page_complaintCaseCollection_salesForce.KnowledgeMaking()).saveLink()
}

def static "page_complaintCaseCollection_salesForce.KnowledgeMaking.kennisTooltipNavigation"(
    	String text	) {
    (new page_complaintCaseCollection_salesForce.KnowledgeMaking()).kennisTooltipNavigation(
        	text)
}

def static "page_caseInfo_salesForce.caseDetailsLeftTab.comboBox"(
    	TestObject path	
     , 	String name	) {
    (new page_caseInfo_salesForce.caseDetailsLeftTab()).comboBox(
        	path
         , 	name)
}

def static "page_caseInfo_salesForce.caseDetailsLeftTab.selectValueFromDropdown"(
    	String txtDropDown	) {
    (new page_caseInfo_salesForce.caseDetailsLeftTab()).selectValueFromDropdown(
        	txtDropDown)
}

def static "page_contactPersons_salesForce.page_createContact.selectContactType"(
    	String contactType	) {
    (new page_contactPersons_salesForce.page_createContact()).selectContactType(
        	contactType)
}

def static "page_contactPersons_salesForce.page_createContact.clickOnVolgende"() {
    (new page_contactPersons_salesForce.page_createContact()).clickOnVolgende()
}

def static "page_contactPersons_salesForce.CaseTab.clickonCaseLink"() {
    (new page_contactPersons_salesForce.CaseTab()).clickonCaseLink()
}

def static "page_contactPersons_salesForce.CaseTab.clickonFirstCase"() {
    (new page_contactPersons_salesForce.CaseTab()).clickonFirstCase()
}

def static "page_consoleTabs_salesForce.consoleTabs.salesForceSectionList"(
    	String section_name	) {
    (new page_consoleTabs_salesForce.consoleTabs()).salesForceSectionList(
        	section_name)
}

def static "page_consoleTabs_salesForce.consoleTabs.clickMainTab"() {
    (new page_consoleTabs_salesForce.consoleTabs()).clickMainTab()
}

def static "page_consoleTabs_salesForce.consoleTabs.clickTab"(
    	String caseNumber	) {
    (new page_consoleTabs_salesForce.consoleTabs()).clickTab(
        	caseNumber)
}

def static "page_consoleTabs_salesForce.consoleTabs.closeTab"() {
    (new page_consoleTabs_salesForce.consoleTabs()).closeTab()
}

def static "page_rapporten_salesForce.allReportsBody.selectReportWithName"(
    	String report_name	) {
    (new page_rapporten_salesForce.allReportsBody()).selectReportWithName(
        	report_name)
}

def static "page_rapporten_salesForce.allReportsBody.selectSubReportWithName"(
    	String report_name	) {
    (new page_rapporten_salesForce.allReportsBody()).selectSubReportWithName(
        	report_name)
}

def static "page_Login_Salesforce.navigateAndEnteringUserCredentials.navigateToURL"(
    	String Url	) {
    (new page_Login_Salesforce.navigateAndEnteringUserCredentials()).navigateToURL(
        	Url)
}

def static "page_Login_Salesforce.navigateAndEnteringUserCredentials.userDetails"(
    	String userName	
     , 	String password	) {
    (new page_Login_Salesforce.navigateAndEnteringUserCredentials()).userDetails(
        	userName
         , 	password)
}

def static "page_caseInfo_salesForce.KnowledgeTab.clickSearchBarKnowledge"() {
    (new page_caseInfo_salesForce.KnowledgeTab()).clickSearchBarKnowledge()
}

def static "page_caseInfo_salesForce.KnowledgeTab.sendTextSearchBarKnowledge"(
    	String KnowledgeValue	) {
    (new page_caseInfo_salesForce.KnowledgeTab()).sendTextSearchBarKnowledge(
        	KnowledgeValue)
}

def static "page_caseInfo_salesForce.KnowledgeTab.clickOnKnowledgeDrpDwn"() {
    (new page_caseInfo_salesForce.KnowledgeTab()).clickOnKnowledgeDrpDwn()
}

def static "page_caseInfo_salesForce.KnowledgeTab.clickOn2ndValueKnowledgeDrpDwn"() {
    (new page_caseInfo_salesForce.KnowledgeTab()).clickOn2ndValueKnowledgeDrpDwn()
}

def static "page_caseInfo_salesForce.KnowledgeTab.clickOnVelden"() {
    (new page_caseInfo_salesForce.KnowledgeTab()).clickOnVelden()
}

def static "page_caseInfo_salesForce.KnowledgeTab.btnKnowledgeArticleDrpDwn"() {
    (new page_caseInfo_salesForce.KnowledgeTab()).btnKnowledgeArticleDrpDwn()
}

def static "page_caseInfo_salesForce.KnowledgeTab.clickOnArtikel_loskoppelen"() {
    (new page_caseInfo_salesForce.KnowledgeTab()).clickOnArtikel_loskoppelen()
}

def static "page_CustomerInfo_salesForce.headerDetails.clickOnNewCase"() {
    (new page_CustomerInfo_salesForce.headerDetails()).clickOnNewCase()
}

def static "page_kennis_salesforce.headerTab.clickOnBewerken"() {
    (new page_kennis_salesforce.headerTab()).clickOnBewerken()
}

def static "page_kennis_salesforce.headerTab.confirmBewerken"() {
    (new page_kennis_salesforce.headerTab()).confirmBewerken()
}

def static "page_rapporten_salesForce.reportsDetails.reportList"(
    	String report	) {
    (new page_rapporten_salesForce.reportsDetails()).reportList(
        	report)
}

def static "page_rapporten_salesForce.reportsDetails.mappenList"(
    	String report	) {
    (new page_rapporten_salesForce.reportsDetails()).mappenList(
        	report)
}

def static "page_complaintCaseCollection_salesForce.ChangeOwner.sendText"(
    	String caseOwner	) {
    (new page_complaintCaseCollection_salesForce.ChangeOwner()).sendText(
        	caseOwner)
}

def static "page_complaintCaseCollection_salesForce.ChangeOwner.clickOwnerWachtrijen"() {
    (new page_complaintCaseCollection_salesForce.ChangeOwner()).clickOwnerWachtrijen()
}

def static "page_complaintCaseCollection_salesForce.ChangeOwner.changeOwnerDrpDwn"() {
    (new page_complaintCaseCollection_salesForce.ChangeOwner()).changeOwnerDrpDwn()
}

def static "page_caseInfo_salesForce.header_CurrentStep.clickOnCurrentStep"(
    	String StepName	) {
    (new page_caseInfo_salesForce.header_CurrentStep()).clickOnCurrentStep(
        	StepName)
}

def static "page_caseInfo_salesForce.header_CurrentStep.markAsCurrentStep"() {
    (new page_caseInfo_salesForce.header_CurrentStep()).markAsCurrentStep()
}

def static "page_complaintCaseCollection_salesForce.createACase.selectRecordType"() {
    (new page_complaintCaseCollection_salesForce.createACase()).selectRecordType()
}

def static "page_complaintCaseCollection_salesForce.createACase.clickOnVolgende"() {
    (new page_complaintCaseCollection_salesForce.createACase()).clickOnVolgende()
}

def static "page_complaintCaseCollection_salesForce.createACase.sendCaseDetails"(
    	java.util.List<WebElement> caseData	
     , 	String CaseInfo	
     , 	String name	) {
    (new page_complaintCaseCollection_salesForce.createACase()).sendCaseDetails(
        	caseData
         , 	CaseInfo
         , 	name)
}

def static "page_complaintCaseCollection_salesForce.createACase.caseTooltipNavigation"(
    	String text	) {
    (new page_complaintCaseCollection_salesForce.createACase()).caseTooltipNavigation(
        	text)
}

def static "page_complaintCaseCollection_salesForce.createACase.selectValueFromDropdown"(
    	String txtDropDown	) {
    (new page_complaintCaseCollection_salesForce.createACase()).selectValueFromDropdown(
        	txtDropDown)
}

def static "page_complaintCaseCollection_salesForce.createACase.winkelSearchDropdown"(
    	String userName	) {
    (new page_complaintCaseCollection_salesForce.createACase()).winkelSearchDropdown(
        	userName)
}

def static "page_caseInfo_salesForce.mijlpalen.clickOnMeerTonen"() {
    (new page_caseInfo_salesForce.mijlpalen()).clickOnMeerTonen()
}

def static "com.helper.validation.validationNverification.verifyTextEqual"(
    	String actualxPath	
     , 	String expectedText	) {
    (new com.helper.validation.validationNverification()).verifyTextEqual(
        	actualxPath
         , 	expectedText)
}

def static "com.helper.validation.validationNverification.verifyObjectEqual"(
    	Object actual	
     , 	Object expected	) {
    (new com.helper.validation.validationNverification()).verifyObjectEqual(
        	actual
         , 	expected)
}

def static "com.helper.validation.validationNverification.verifyElementNotClikable"(
    	TestObject obj	) {
    (new com.helper.validation.validationNverification()).verifyElementNotClikable(
        	obj)
}

def static "com.helper.validation.validationNverification.countElements"(
    	TestObject Obj_count	) {
    (new com.helper.validation.validationNverification()).countElements(
        	Obj_count)
}

def static "page_CustomerInfo_salesForce.DuplicateTab.clickOnDuplicateAccountLink"() {
    (new page_CustomerInfo_salesForce.DuplicateTab()).clickOnDuplicateAccountLink()
}

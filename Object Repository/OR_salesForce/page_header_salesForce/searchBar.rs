<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>searchBar</name>
   <tag></tag>
   <elementGuidId>2df70698-7c4b-43f5-b9b3-fc2dc19e7f57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#436:0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;oneHeader&quot;]//div[@class=&quot;uiInput uiAutocomplete uiInput--default&quot;]/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>

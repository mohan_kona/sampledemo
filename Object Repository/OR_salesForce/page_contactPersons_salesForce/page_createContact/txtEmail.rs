<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtEmail</name>
   <tag></tag>
   <elementGuidId>7b54d76b-a26e-455c-ab18-42498af83bfc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@class=&quot; input&quot;]//preceding::label[1]/span[text()=&quot;E-mail&quot;]//following::input[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RadioBtnContactType</name>
   <tag></tag>
   <elementGuidId>6fb79133-e989-4c4e-9fa4-9f501799a514</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class=&quot;changeRecordTypeLeftBody &quot;]//legend//following::div[1]/label[1]/span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;changeRecordTypeLeftBody &quot;]//legend//following::div[1]/label[1]/span[1]</value>
   </webElementProperties>
</WebElementEntity>

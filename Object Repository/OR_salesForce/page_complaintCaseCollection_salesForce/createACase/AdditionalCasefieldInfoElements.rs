<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AdditionalCasefieldInfoElements</name>
   <tag></tag>
   <elementGuidId>7ae8e7cb-e570-4631-9dd2-bb8a9cd8ca17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/h3/span[text()='Aanvullende gegevens']//following::div[@class='slds-grid full forcePageBlockSectionRow']/div//a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>

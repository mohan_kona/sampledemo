<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnOmniChannel</name>
   <tag></tag>
   <elementGuidId>abde44c4-cd94-496f-8b65-2044b9620003</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@class=&quot;utilitybar slds-utility-bar&quot;]//span[text()=&quot;Omni-Channel&quot;]//ancestor::button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
